#!/bin/bash

home_dir="/home/lampa"					# домашний коталог
save_bak_dir_dropbox="$home_dir/Dropbox" 		# Метсо нахождения dropbox
save_bak_dir="/media/lampa/EACF-16EA" 			# флешка телефона
# время
DATEYMDY=`date "+%Y"`
DATEYMDM=`date "+%m"`

#-----------------------------------------

#
# Копирую телефонные разговоры
#

echo "Dropbox"
echo
echo "Запись звонков"
mkdir -p $save_bak_dir_dropbox/"Backup/Android/smartphone/Phone/Call Record"/$DATEYMDY/$DATEYMDM
mv  $save_bak_dir/CallRecordings/* $save_bak_dir_dropbox/"Backup/Android/smartphone/Phone/Call Record"/$DATEYMDY/$DATEYMDM
echo

