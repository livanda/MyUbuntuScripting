#!/bin/sh

dpkg --configure -a
apt-get -f install
KEYS=`sudo apt-get update | awk '/NO_PUBKEY/ {print($NF)}'`		# Выполняем обновление

if [ -z $KEYS ]
then
    echo "Ключь НЕНАЙДЕН."
else
    echo "Ключь Найден : $KEYS"
    for KEY in $KEYS
    do
        gpg --keyserver keyserver.ubuntu.com --recv $KEY &&\		# Вставляем ключь 
        gpg --export --armor $KEY | sudo apt-key add -- &&\		# Вставляем ключьщ

        echo "Public key $KEY has been added."
    done
fi
#wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - 
apt-get update 2> /dev/null
apt-get -y upgrade
apt-get -y autoremove
apt-get -y dist-upgrade
apt-get -y clean
gem update
npm -g up
