#!/bin/bash
dir="/media/livanda/My_Hdd/Game/openkore" # Директория где лежит устновленная кора
dir_inst="/media/livanda/My_Hdd/Game/Ragnarok-openkore/"
install="sudo apt-get -y install" # Установка пакетов
mkdir -p $dir $dir_inst
cd $dir_inst # Переходим в где лежит, кора.
#--------------------------------------#
#        Инсталируем нужные            #
#                                      #
#             пакеты                   #
#--------------------------------------#

#$insall perl
#$insall libcarp-assert-perl
#$insall libreadline*-dev
#$insall build-essential
#$insall g++
#$insall libcurl*-dev

#
wget -c http://openkore.collectskin.com/openkore_ready.zip
unzip openkore_ready.zip
rm openkore_ready.zip
cd home/openkore/svnsnapshots
rar a -m5 openkore_ready.rar openkore_ready
cd $dir_inst
mv home/openkore/svnsnapshots/*.rar .

#
wget -c http://openkore.collectskin.com/openkore.zip
unzip openkore.zip
rm openkore.zip
cd home/openkore/svnsnapshots
rar a -m5 openkore.rar openkore
cd $dir_inst
mv home/openkore/svnsnapshots/*.rar .

#
wget -c http://openkore.collectskin.com/confpack.zip
unzip confpack.zip
rm confpack.zip
cd home/openkore/svnsnapshots
rar a -m5 confpack.rar confpack
cd $dir_inst
mv home/openkore/svnsnapshots/*.rar .

#
wget -c http://openkore.collectskin.com/tablepack.zip
unzip tablepack.zip
rm tablepack.zip
cd home/openkore/svnsnapshots
rar a -m5 tablepack.rar tablepack
cd $dir_inst
mv home/openkore/svnsnapshots/*.rar .

#
wget -c http://openkore.collectskin.com/fieldpack.zip
unzip fieldpack.zip
rm fieldpack.zip
cd home/openkore/svnsnapshots
rar a -m5 fieldpack.rar fieldpack
cd $dir_inst
mv home/openkore/svnsnapshots/*.rar .
rm -rf home
#



cd $dir

#
cp "$dir_inst"openkore.rar .
unrar x openkore.rar
rm openkore.rar
mkdir -p openkore/control openkore/tables openkore/fields
#
cp "$dir_inst"confpack.rar .
unrar x confpack.rar openkore/control 
rm confpack.rar
mv $dir/openkore/control/confpack/control/*.txt $dir/openkore/control
#
cp "$dir_inst"tablepack.rar .
unrar x tablepack.rar openkore/tables
rm tablepack.rar

cp -r $dir/openkore/tables/tablepack/tables/* $dir/openkore/tables
rm -rf $dir/openkore/tables/tablepack/tables
cp "$dir_inst"fieldpack.rar . ; unrar x fieldpack.rar openkore/fields
rm fieldpack.rar
mv $dir/openkore/fields/fieldpack/fields/* $dir/openkore/fields

rar a -m5 openkore.rar openkore
#cd openkore ; perl ./openkore.pl

