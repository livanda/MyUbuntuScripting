#!/bin/bash

#
#	Скрипт для копирования дисков
#	Перед спотзованием установите
#	Пакет pv sudo apt-get -y install pv
#

# CD/DVD Привод
DISK_DEV=/dev/sr0
 
# Папка в которую будут сохранятся образы
IMGS_DIR="/home/livanda/1/CD"
mkdir -p "$IMGS_DIR"
 
[ -d "$IMGS_DIR" ] || { echo "Нет такой папки"; exit 1; }
while :; do
        clear; echo
        if [ "$(udisksctl info --block-device $DISK_DEV | grep "^    Size:" | grep -o "[0-9]*")" -gt 0 ]
        then
                sync
                DISC_NAME="$(isoinfo -d -i $DISK_DEV | grep "^Volume id" | sed 's/Volume id: //')"
                [ "$DISC_NAME" ] || { echo "Не удалось получить имя диска, введите его сами:"; read DISC_NAME; }
                N=2
                while [ -f "$IMGS_DIR/$DISC_NAME.iso" ]; do
                        let N+=1
                        DISC_NAME="$DISC_NAME$N"
                done
                echo "Чтение диска в образ $IMGS_DIR/$DISC_NAME.iso"
#                dd if=$DISK_DEV of="$IMGS_DIR/$DISC_NAME.iso"
               dd if=$DISK_DEV |pv| dd of="$IMGS_DIR/$DISC_NAME.iso"
#		sudo mount -t iso9660 -o ro /dev/cdrom /mnt ; cp -rfv /mnt/* /home/livanda/2/ ; sudo umount /dev/cdrom ;chmod -R 777 /home/livanda/2/
		chmod -R 777 "$IMGS_DIR"/*
                echo -e "завершено.\nВставте следуюший диск и нажмите Enter."
                eject
#               read
                eject -t
        else
                echo "Нет диска"
        fi
        sleep 2
done
 
