#!/bin/bash
ins="sudo apt-get -y install"

$ins lamp-server
$ins apache2
$ins apache2-mpm-prefork
$ins apache2-utils
$ins apache2.2-common
$ins libapache2-mod-php5
$ins libapr1
$ins libaprutil1
$ins libdbd-mysql-perl
$ins libdbi-perl
$ins libmysqlclient15off
$ins libnet-daemon-perl
$ins libplrpc-perl
$ins libpq5
$ins mysql-client-5.0
$ins mysql-common
$ins mysql-server
$ins mysql-server-5.0
$ins php5-common
$ins php5-mysql
$ins mysql-server
$ins mysql-client
$ins libmysqlclient15-dev
$ins apache2
$ins apache2-doc
$ins apache2-mpm-prefork
$ins apache2-utils
$ins libexpat1
$ins ssl-cert
$ins libapache2-mod-php5
$ins libapache2-mod-ruby
$ins php5 php5-common
$ins php5-curl
$ins php5-dev
$ins php5-gd
$ins php5-idn
$ins php-pear
$ins php5-imagick
$ins php5-imap
$ins php5-mcrypt
$ins php5-memcache
$ins php5-mhash
$ins php5-ming
$ins php5-mysql
$ins php5-pspell
$ins php5-recode
$ins php5-snmp
$ins php5-sqlite
$ins php5-tidy
$ins php5-xmlrpc
$ins php5-xsl
$ins phpmyadmin
# Настройка phpmyadmin
sudo dpkg-reconfigure -plow phpmyadmin
sudo ln -s /etc/phpmyadmin/apache.conf /etc/apache2/conf.d/phpmyadmin.conf
sudo /etc/init.d/apache2 reload
sudo ln -s /etc/phpmyadmin/apache.conf /etc/apache2/conf-available/phpmyadmin.conf
sudo a2enconf phpmyadmin
sudo /etc/init.d/apache2 reload
#cd /var/www/
#sudo svn checkout https://phpmyadmin.svn.sourceforge.net/svnroot/phpmyadmin/tags/STABLE/phpMyAdmin phpMyAdmin
#cd phpMyAdmin
#sudo mkdir config
#sudo chmod o+rw config
git clone git://github.com/jquery/jquery.git
